import "../style/Paginator.css";
import { useState } from "react";

const Paginator = ({ clientsPerPage, totalClients, paginate }) => {
  const pageNumbers = [];

  const [activePage, setActivePage] = useState(1)

  for (let i = 1; i <= Math.ceil(totalClients / clientsPerPage); i++) {
    pageNumbers.push(i);
  }
  
  return (
    <div className="pagination">
      <div>
        <a>&laquo;</a>
        {pageNumbers.map((number) => (
          <a
            key={number}
            onClick={()=>{ setActivePage(number)}}
            className={number === activePage? 'active':''}
          >
            {number}
            {paginate(activePage)}
          </a>
        ))}
        <a>&raquo;</a>
      </div>
    </div>
  );
};

export default Paginator;
