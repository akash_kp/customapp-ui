import "../style/MainHeader.css";
import logo from "../logo.jpg";
import userImage from "../user.jpg";
import { Button } from "primereact/button";
import "primereact/resources/themes/lara-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css"; //icons
import { Avatar } from "primereact/avatar";
import { userContext } from "../App";
import React, { useContext, useRef } from "react";
import { useHistory } from "react-router-dom";
import { Menu } from "primereact/menu";

const MainHeader = () => {
  const history = useHistory();
  const menu = useRef(null);

  const navigateToHome = () => {
    history.push("/HomeScreen");
  };

  const handleLogOut = () => {
    sessionStorage.removeItem("user-name");
    sessionStorage.removeItem("user-id");
    history.push("/LogIn");
  };

  let menuOptions = [
    { label: "View Profile", icon: "pi pi-user" },
    { label: "Contact Us", icon: "pi pi-comment" },
    {
      label: "Log Out",
      icon: "pi pi-sign-out",
      command: () => {
        handleLogOut();
      },
    },
  ];
  return (
    <div className="header-container">
      <div className="left-content-container">
        <img src={logo} alt="company logo"></img>
        <label className="beminus">Beminus</label>
        <Button
          id="btn"
          className="p-button-text p-button-secondary"
          label="Dashboard"
          icon="pi pi-home"
          onClick={navigateToHome}
        />
      </div>

      <div className="right-content-container">
        <img
          src={userImage}
          alt="user profile"
          style={{ borderRadius: "90%", marginRight: "0" }}
        ></img>
        <label className="user-name">
          {sessionStorage.getItem("user-name")}
        </label>
        <div class="dropdown">
          <Menu model={menuOptions} popup ref={menu} />
          <Button
          id = "btn"
            className="p-button-text p-button-secondary"
            onClick={(event) => menu.current.toggle(event)}
            icon="pi pi-align-justify"
            style={{marginTop : "17%"}}
          />
        </div>
      </div>
    </div>
  );
};

export default MainHeader;
