import '../style/MainFooter.css'

const MainFooter = () => {
    return(
        <div>
            <label>Powered by Beminus Group</label>
            <div className="copyright-content"><label >@ 2022 Beminus Group. All rights reserved</label></div>
        </div>
    )
}

export default MainFooter