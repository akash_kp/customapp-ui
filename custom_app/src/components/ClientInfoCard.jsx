import '../style/ClientInfoCard.css'
import { Card } from 'primereact/card';


const ClientInfoCard = (props) =>{
    return(
        
            <Card title={props.client.name} subTitle={props.client.industry}>
         
        <div>{props.client.city}</div>
        <div>{props.client.state}</div>
</Card>
      
    )
}

export default ClientInfoCard