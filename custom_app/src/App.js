import "./App.css";
import { Route } from "react-router-dom";
import LogIn from "./page/LogIn";
import HomeScreen from "./page/HomeScreen";
import AddClient from "./page/AddClient";

import React from "react";
import { useState } from "react";


export const userContext = React.createContext()


function App() {

  const [userName, setUserName] = useState('')
  const value = {userName, setUserName}

  return (
    <userContext.Provider value={value}>
    <div className="App">
      <Route path='/LogIn'><LogIn/></Route>
      <Route path='/HomeScreen'><HomeScreen/></Route>
      <Route path='/AddClient'><AddClient/></Route>
    </div>
    </userContext.Provider>
  );
}

export default App;
