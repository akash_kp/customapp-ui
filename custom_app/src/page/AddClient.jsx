import MainHeader from "../components/MainHeader";
import MainFooter from "../components/MainFooter";
import "../style/AddClient.css";
import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { useRef } from "react";
import { Toast } from "primereact/toast";

const AddClient = () => {
  const enteredComapanyName = useRef("");
  const enteredRegion = useRef("");
  const enteredCity = useRef("");
  const enteredPerson = useRef("");
  const enteredIndustry = useRef("");
  const enteredPosition = useRef("");
  const enteredEmail = useRef("");
  const enteredState = useRef("");
  const toast = useRef(null);

  const showMessage = (errorMessage, type) => {
    toast.current.show({ severity: type, summary: "", detail: errorMessage });
  };

  const handleReset = () => {
    enteredComapanyName.current.value = "";
    enteredRegion.current.value = "";
    enteredCity.current.value = "";
    enteredPerson.current.value = "";
    enteredIndustry.current.value = "";
    enteredPosition.current.value = "";
    enteredEmail.current.value = "";
    enteredState.current.value = "";
  };

  const isValidateInput = () => {
    if (
      enteredComapanyName.current.value === "" ||
      enteredIndustry.current.value === "" ||
      enteredCity.current.value === "" ||
      enteredState.current.value === ""
    ) {
      showMessage("Mandatory details missing", "warn");
      return false;
    }
    return true;
  };

  async function saveClent(clientOrganiztion) {
    await fetch("http://localhost:8080/api/org/saveOrg", {
      method: "POST",
      headers : {'Content-Type':'application/json'},
      body: JSON.stringify(clientOrganiztion)
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error();
      })
      .then((responseJson) => {
        showMessage("New Client Added", "success");
        handleReset()
      })
      .catch((error) => {
        showMessage("Something went wrong", "error");
      });
  }

  const handleSubmit = () => {
    if (!isValidateInput()) return;

    const clientOrganiztion = {
      name: enteredComapanyName.current.value,
      industry: enteredIndustry.current.value,
      city: enteredCity.current.value,
      state: enteredState.current.value,
      managedByUser: sessionStorage.getItem("user-id"),
    };

    saveClent(clientOrganiztion);
  };

  return (
    <div className="main-container">
      <div className="header-box">
        <MainHeader />
      </div>
      <div className="content-heading-box">
        <p className="p1">New Client</p>
        <p>
          When you complete the process, this customer will be added to your
          client list
        </p>
      </div>
      <div className="add-client-content-box">
        <p> Add your clients details</p>
        <div className="input-container-grid">
          <p className="comany-details">Company Details</p>
          <div className="input-grid">
            <div>
              <p>Company Name<label className="mandatory">*</label></p>
              <div className="p-inputgroup">
                <InputText ref={enteredComapanyName} />
              </div>
            </div>
            <div>
              <p>Region</p>
              <div className="p-inputgroup">
                <InputText ref={enteredRegion} />
              </div>
            </div>
            <div>
              <p>City<label className="mandatory">*</label></p>
              <div className="p-inputgroup">
                <InputText ref={enteredCity} />
              </div>
            </div>
            <div>
              <p>Contact Person</p>
              <div className="p-inputgroup">
                <InputText ref={enteredPerson} />
              </div>
            </div>
            <div>
              <p>State<label className="mandatory">*</label></p>
              <div className="p-inputgroup">
                <InputText ref={enteredState} />
              </div>
            </div>
            <div>
              <p>Industry<label className="mandatory">*</label></p>
              <div className="p-inputgroup">
                <InputText ref={enteredIndustry} />
              </div>
            </div>
            <div>
              <p>Email</p>
              <div className="p-inputgroup">
                <InputText ref={enteredEmail} />
              </div>
            </div>
            <div>
              <p>Position</p>
              <div className="p-inputgroup">
                <InputText ref={enteredPosition} />
              </div>
            </div>
            <div className="empty-spacing-box"></div>
            <div className="btn-box">
              <Button
                className="p-button-secondary"
                label="Clear"
                onClick={handleReset}
              />
              <Button
                className="p-button-secondary"
                label="Save client"
                onClick={handleSubmit}
              />
            </div>
          </div>
        </div>
      </div>

      <div className="footer-box">
        <MainFooter />
        <Toast ref={toast} />
      </div>
    </div>
  );
};

export default AddClient;
