import "../style/HomeScreen.css";
import MainHeader from "../components/MainHeader";
import MainFooter from "../components/MainFooter";
import "primereact/resources/themes/lara-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css"; //icons
import React, { useState, useEffect } from "react";
import { Button } from "primereact/button";
import { useHistory } from "react-router-dom";
import ClientInfoCard from "../components/ClientInfoCard";
import Paginator from "../components/Paginator";

function useWindowWidth(){
  const[size, setSize] = useState(window.innerWidth)
  console.log('enetering resize ' +window.innerWidth)
  useEffect(()=>{
    const handleResize =() =>{
      setSize(window.innerWidth)
    };
    window.addEventListener('resize',handleResize)
    return () => {
      window.removeEventListener('resize',handleResize)
    }
  },[])

  return size
}

const HomeScreen = () => {
  const history = useHistory();
  const [allClientOrganizations, setClientOrganizations] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [dataPerPage, setDataPerPage] = useState(12);

  const screenWidth = useWindowWidth()

  useEffect(()=>
 
  { 
    if(screenWidth > 1000){
      setDataPerPage(12)
    }else if(screenWidth > 661){
      setDataPerPage(6)
    }else{
      setDataPerPage(5)
    }
      
  },[screenWidth])
  
  const navigateToAddClient = () => {
    history.push("/AddClient");
  };

  async function getAllClientOrganizations() {
   const response = await fetch(
      "http://localhost:8080/api/org/getOrg/" +
        sessionStorage.getItem("user-id"),
      {
        method: "GET",
      }
    )

    if ( await response.status === 500){

    }else{
      setClientOrganizations(await response.json())
    }
      
  }

  useEffect(() => {
   getAllClientOrganizations()
  }, []);

  const indexOfLastClient = currentPage * dataPerPage;
  const indexOfFirstClient = indexOfLastClient - dataPerPage;
  const currentClients = allClientOrganizations.slice(indexOfFirstClient, indexOfLastClient);
  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div className="main">
      <div className="header-box">
        <MainHeader />
      </div>
      <div className="add-client-box">
        <div className="welcome-heading">
          <p className="p1">
            {"Welcome Back, " + sessionStorage.getItem("user-name")}
          </p>
          <p>We are pleased to see you</p>
        </div>
        <div className="btn-container">
          <Button
            className="p-button-secondary"
            id="add-client-btn"
            label="Add client"
            icon="pi pi-plus"
            onClick={navigateToAddClient}
          />
        </div>
      </div>
      <div className="content-box">
        {currentClients.map(client => 
          <ClientInfoCard key = {client.id} client={client} />
        )}
      </div>
      <Paginator clientsPerPage={dataPerPage}  totalClients={allClientOrganizations.length}    paginate={paginate}/>
      <div className="footer-box">
        <MainFooter />
      </div>
    </div>
  );
};

export default HomeScreen;
