import "../style/LogInLayout.css";
import "primereact/resources/themes/lara-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css"; //icons

import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Messages } from "primereact/messages";
import { useHistory } from 'react-router-dom';
import React, {useRef,useContext } from "react";
import { userContext } from "../App";

const LogIn = () => {
  const enteredEmail = useRef("");
  const enteredPassword = useRef("");
  const messages = useRef(null);
  const history = useHistory();
  const {userName, setUserName} = useContext(userContext)

  async function doLogin(userEmail, password) {
    const response = await fetch("http://localhost:8080/api/user/logIn", {
      method : 'POST',
      headers: { email: userEmail, password: password },
    });

    if ( await response.status === 500){
      showError(await response.text())
    }else{
      const user = await response.json()
      sessionStorage.setItem('user-name',user.userName)
      sessionStorage.setItem('user-id',user.id)
      setUserName(user.userName)
      history.push('/HomeScreen')
    }
  
  }

  const handleSubmit = () => {
    if (enteredEmail.current.value === "" || enteredPassword.current.value === "")
      showError('Invalid Input');
    else {
      doLogin(enteredEmail.current.value, enteredPassword.current.value);
    }
  };

  const showError = (errorMessage) => {
    messages.current.show({
      severity: "error",
      summary: "",
      detail:' '+errorMessage,
    });
  };

  return (
    <div className="container">
      <div className="emtptyBox"></div>
      <div className="box">
        <div className="logInContainer">
          <div className="logInBox1">
            <p className="firstHeading">Welcome to Beminus </p>
            <p>Please enter your details</p>
          </div>
          <div className="logInBox">
            <p>Email</p>
            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">
                <i className="pi pi-envelope"></i>
              </span>
              <InputText placeholder="Enter your Email" ref={enteredEmail} />
            </div>
          </div>
          <div className="logInBox">
            <p>Password</p>

            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">
                <i className="pi pi-lock"></i>
              </span>
              <InputText
                placeholder="Enter your Password"
                type="password"
                ref={enteredPassword}
              />
            </div>
          </div>
          <div className="logInBox">
            <Button
              className="p-button-secondary"
              label="Submit"
              onClick={handleSubmit}
            />
          </div>
          <Messages ref={messages}></Messages>
        </div>
      </div>
    </div>
  );
};

export default LogIn;
